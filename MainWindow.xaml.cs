﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WpfGameOfLife
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            DrawGrid(100, 0, 0, 1);
        }
        
        public void DrawGrid(byte r, byte g, byte b, byte thickness)
        {
            for (int i = 0; i <= 1000; i+=100)
            {
                SolidColorBrush brush = new SolidColorBrush(Color.FromRgb(r, g, b));
                Line lineX = new Line();
                Line lineY = new Line();
                lineX.X1 = i;
                lineX.X2 = i;
                lineX.Y1 = 0;
                lineX.Y2 = 1000;
                lineX.Stroke = brush;
                lineX.StrokeThickness = thickness;
                lineY.X1 = 0;
                lineY.X2 = 1000;
                lineY.Y1 = i;
                lineY.Y2 = i;
                lineY.Stroke = brush;
                lineY.StrokeThickness = thickness;
                canvasBackground.Children.Add(lineY);
                canvasBackground.Children.Add(lineX);
            }
        }
    }
}
